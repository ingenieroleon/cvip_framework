# README 2019-03-24 #

# Framework Desarollado por [Colombia Vip.](https://www.colombiavip.com)

Framework desarrollado tomando lo mejor de diferentes estilos de arquitectura de software, principalmente MVC (model-view-controller).

### ¿Como configurar? ###

* Por definir...


### Modo de Uso ###

* Por definir...

### Documentación ###

* Metodo por default para la page_menu principal: principal().
* Metodo por default para las sub_pagge: listar().
* ...

## Advertencias ##

* Si dos columnas de una misma tabla se llaman igual, Footable solo mostrara la primera que encuentre de izquierda a derecha, Ej: ID | ID. Por lo que se recomienda usar nombre unicos para cada columna.

## En Construcción ##

* Relaciones Belongs to.
* Relaciones Has many.
* Mejoras Shortcodes.
* Modificaciones al apartado de editar, eliminar y agregar por las relaciones Belongs to.
* Creación de modelos automatizado.
* Permisos.
* Vistas Publicas.

## Problemas Actuales ##

* Editar, Eliminar y Agregar han dejado de funcionar debido a las relaciones Belongs To.
* Los CSS y/o los JS no funcionan en el frontend.
* Las rutas de las vistas publicas pueden general confusion al tener varios Plugin que extiendan al Framework, debido a que el framework toma la primera ruta existente.
Posible solucion: WEB/NOMBRE_PLUGIN/CONTROLADOR/OPCION
* Las key primarias pueden variar, hacer que eso no importe y hacer funcionar en cualquier caso.

## Solucionado ##

###20190513

* Se agrega la posibilidad de que el plugin externo agrege Js y Styles. Estos estaran en una sub carpeta llamada 'assets' del directiorio raiz del plugin; los css y js estaran separados por sus respectivas carpetas y se cargaran automaticamente.
* Mejoras a la funcion 'loadPubController' para que fuera compatible con los shortcodes.
* Ahora los shortcodes solo muestran vistas publicas.

### 20190511

* Se unifica el archivo 'iniciador' con 'cvip_framework'.
* Los archivos del Framework se movieron a la raiz del plugin.
* Correcciones con los formularios AJAX.
* Las opciones para el controlador backend ahora se toman con la variable 'option'.
* Se eliminan variables globales innecesarias.
* Los shortcode ahora funcionan. Ejemplo de uso: `[cvip_mvc c="wp_posts"  o="listar" plugin="plugin-prueba"]`
Si no se le pasa la opción, usara listar.

### 20190505

* Se agregan las vistas publicas.

### 20190430

* Correción de rutas 'page=cvip_MODELEO'.
* Se modifica el plugin de ejemplo, ahora extiende la clase cvip_framework y se corrige un error.
* Se agrega un plugin de ejemplo.
* Se cambia el nombre de algunas funciones agregandole el prefijo 'cvip_NOMBRE_FUNCION'

### 20190428

#### Se separa la carpeta App del framework.

#### Estructura de archivos que debe tener el plugin que utilizara el framework.

* app
  - controllers
  - models
  - views
* plugin-Prueba.php
* index.php

#### Contenido del archivo 'plugin-prueba.php'.

~~~~
<?php
/*
 * Plugin Name: prueba-plugin
 * Plugin URI: ColombiaVIP.com
 * Description: Este es un plugin de prueba.
 * Version: 1.0
 * Author: ColombiaVIP.com
 * Author URI: ColombiaVIP.com
 */

 defined ('ABSPATH') or die ("Ey!, no deberias estar aqui");

 if (class_exists('cvip_framework')) {
   class pluginPrueba extends cvip_framework{

     function __construct() {
       parent::__construct(__DIR__);
       # = ACTIVACION DEL PLUGIN  =
       register_activation_hook(__FILE__, array($this, 'activate'));
       # = DESACTIVACION DEL PLUGIN =
       register_deactivation_hook(__FILE__, array($this, 'deactivate'));
     }

     function activate(){
       $this->cvip_activate();
     }

     function deactivate(){
       $this->cvip_deactivate();
     }

   }

   new pluginPrueba();
 }
~~~~

### 20190421

* Ajustes y correcciones al metodo de editar y modificaciones a la clase html que causaba un bucle infinito llamando al controlador y a su respectivo metodo una y otra vez.

### 20190411

* Se agrega el apartado de Usuarios, ahora al activar el plugin se crea un Usuario superior con el nombre del plugin, se agrega este rol al administrador y adicional a eso se crea un usuario por cada modelo.

### 20190403

* Los Has Many ahora se muestran en columnas separadas de la principal y se muestran en el orden definido por el array Has Many.

### 20190402

* Primeras pruebas con Has May.

### 20190331

* De momento los permisos se dejan como administrador para los SUB_PAGE, mientras se solucionan los permisos.
* Cuando la tabla no existe, mata el proceso y suelta un mensaje de error.

### 20190330

* Mejoras a las relaciones Belongs to, ahora funciona como un array que contienen el nombre de la tabla, la keyExterna y los campos a traer.

### < 20190330

* Rutas: /controlador/metodo/opcion
* Las rutas dentro del framework.
