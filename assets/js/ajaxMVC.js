  function ajaxMVC (postData, contenedor='.contenedor'){
    //SI NO ES POSTDATA LO RECORRO Y CREO EL POSTDATA
    if (!(postData instanceof FormData)) {
      var data=new FormData();
      // var data=new FormData(jQuery(this));
      for (var elemento in postData) {
        if (postData.hasOwnProperty(elemento)) {
            data.append(elemento, postData[elemento]);
        }
      } postData=data;
    }

    postData.append("action", "cvip_ajax_mvc");

    jQuery.ajax({
             type: "POST",
             url: WP_AJAX.ajaxurl,
             data: postData,
             processData: false,
             contentType: false,
             beforeSend: function(data) {
                jQuery(contenedor).html("<marquee class='marq' bgcolor=silver width=200 height=20 direction=left behavior=alternate>CARGANDO...</marquee>");
             },
             error: function(jqXHR, textStatus, errorMessage) {
               console.log(textStatus);
               console.log(errorMessage);
             },
             success: function(data) {
              jQuery(contenedor).html(data);
             }
         });
  };

  //FUNCION PARA LOS FORMS
  jQuery(document).on("submit","form.asyn", function(event){
    event.preventDefault();
    var postData    =   new FormData(this);
    ajaxMVC(postData);
  });

  jQuery(document).ready(function($) {
      jQuery(document).off('submit',"form.opcionesRegistro"); //OBLIGATORIO SI SE DESEA QUE EL FORM PUEDA CARGARSE DE NUEVO
      jQuery(document).on('submit',"form.opcionesRegistro",function(e){
        // var postData    =   new FormData(this);
        var postData = $(this).serializeArray();
        // postData.append("action", "cvip_salvar");
        postData.push({name: "action", value: "opcionesRegistro"});

        jQuery.ajax({
          type: "POST",
          url: WP_AJAX.ajaxurl,
          data: postData,
          // processData: false,
          // contentType: false,
          success: function(data) {
            alert('Resultado: ' +  data);
            $(".button-volver input").click();
          }
        });
      });
    });


  //FUNCION PARA LOS UPLOADS
  function uploadButton(){

  }

//------------------------------Prubas-------------------------

jQuery(document).on("click",".link-mvc", function(event){
  event.preventDefault();
  console.log("entra");
  var datas = jQuery(this).data();

  if (datas.confirmacion) {
    if (!confirm(datas.confirmacion) ){
      return;
    }
  }

  ajaxMVC({
    "mvc": datas.mvc,
    "id": datas.id,
    "tabla": datas.tabla,
    //Enviar los demas datas
    },
    //'.tabla_' + datas.tabla,
  );


});



function ajaxMV(modelo, actions = { 'opcion':'listar' }){

  var postData = new FormData();
  postData.append("action", "cargarControladoresAjax");
  postData.append("modelo", modelo);

  for (var elemento in actions) {
    postData.append(elemento, actions[elemento]);
  }

  jQuery.ajax({
    type: "POST",
    url: WP_AJAX.ajaxurl,
    data: postData,
    processData: false,
    contentType: false,
    beforeSend: function (data) {
      jQuery('#wpbody-content').html("<marquee class='marq' bgcolor=silver width=200 height=20 direction=left behavior=alternate>CARGANDO...</marquee>");
    },
    error: function (jqXHR, textStatus, errorMessage) {
      console.log(textStatus);
      console.log(errorMessage);
    },
    success: function (data) {
      //alert("result: " + data);
      window.location.href = data;
    }
  });
};
