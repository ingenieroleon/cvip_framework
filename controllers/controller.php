<?php
  # =================================================
  # =             CONTROLADOR PRINCIPAL             =
  # =================================================
class controller {

  public $modelo;
  public $modelos;
  public $nombreModelo;
  public $nombreTabla;
  public $nombreColumnas;
  public $html;
  public $keyPrimary;
  public static $pluginpath;

  function __construct($modelo=NULL, $pluginpath = NULL){

    self::$pluginpath = $pluginpath;

    if ($modelo) {
      $this->nombremodelo = $modelo;
      $this->modelo = $this->modelo($modelo);
      $this->nombreTabla = $this->modelo->nombreTabla();
      $this->nombreColumnas = $this->modelo->nombreColumnas();
      $this->keyPrimary = $this->modelo->primaryKey;
    }
//($this->keyPrimary)??$this->keyPrimary
    $this->html = new cvip_html( ($this->keyPrimary)??$this->keyPrimary );

  }

  function obtenerModelos(){
    global $submenu;

    $temp = $submenu[get_admin_page_title()];
    unset($temp[0]);
    $modelos = [];

    foreach ($temp as $key => $subPages) {
      $modelos[] = $subPages[0];
    }

    return $modelos;
  }

  # = RETORNA OBJETO DEL MODELO =
  function modelo($modelo){
    $tabla = model::validarTabla(strtolower($modelo));
    $rutaModelo = self::$pluginpath."/app/models/$modelo.php";

    !($tabla[0][0])?? die("ERROR: La tabla no existe");

    if (file_exists($rutaModelo)) {
      require_once $rutaModelo;
      if (class_exists($modelo)) {
        return new $modelo();
      }
    }

    //OPCION PARA CREAR MODELO
    //Mostrar Mensaje en la consola
    //die("NO EXISTE EL MODELO");
  }

  public static function vista($nombreVista = '', $datos = array()){
      extract($_REQUEST);
      $rutaArchivo = self::$pluginpath."/app/views/admin/$nombreVista.php";
      $rutaArchivoFW = RUTA_CVIP_FRAMEWORK."/views/admin/$nombreVista.php";

      if (is_array($datos)){
        extract($datos);
      }

      ob_start();
      if (file_exists("$rutaArchivo")){
        require("$rutaArchivo");
      }
      elseif (file_exists($rutaArchivoFW)){
        require($rutaArchivoFW);
      }
      else {
        echo("<br>Error: No existe la vista: ".$nombreVista." <br>");
      }
      return ob_get_clean();
  }

  //FUNCION PRINCIPAL PAGE_MENU
  function principal(){

    $this->modelos = $this->obtenerModelos();

    $datos = $this->modelos;

    echo $this->vista('principal', $datos);

    // funcion a tener en cuenta
    //menu_page_url( SUB_PAGE[0], true );
  }

  # = FUNCION POR DEFECTO, TRAE LOS DATOS DE LA BASE DE DATOS Y LOS LISTA =
  function listar(){

    //MOVER AL CONSTRUCTOR
    $vista = $this->html->crearTabla([
      "tabla"    => $this->nombreTabla,
      "datos" => $this->modelo->DB('ARRAY_A'),
      "buscador" => TRUE,
      "tamano" => '25',
      "modificacion" => TRUE,
    ]);

    echo $this->vista('listar', $vista);
  }

  # = FUNCION PARA EDITAR UN REGISTRO =
  public function editar(){

    $formulario = $this->cud();

    if ($this->modelo->hasMany) {

      $id = $_GET['id'];
      $hasMany = [];

      foreach ($this->modelo->hasMany as $tabla => $datos) {
        //$tablasHasMany []
        $hasMany[] = $this->html->crearTabla([
          "tabla"    => $this->nombreTabla,
          "datos" => $this->modelo->hasMany($tabla, $datos, $id),
          "buscador" => TRUE,
          "tamano" => '25',
          "modificacion" => TRUE,
          "modificacion" => FALSE,
        ]);

      }

    }

    $datos['hasMany'] = $hasMany;

    // echo "<br>";echo "<br>";
    // echo "<pre>Datos: ";
    // print_r($datos);
    // echo "</pre>";
    // echo "<br>";echo "<br>";

    echo $this->vista('cud', [$formulario, $hasMany]);


  }

   # = FUNCION PARA ELIMINAR UN REGISTRO =
  public function eliminar(){
    $formulario = $this->cud();

    echo $this->vista('cud', [$formulario]);
  }

  public function agregar (){//UNIFICARLO CON EDITAR Y ELIMINAR
    $this->cud();
  }

  private function cud(){ // CREATE, UPDATE, DELETE

    $nombreColumnas = $this->nombreColumnas;

    //la tabla deberia ser validada en el modelo
    $infoTable = $this->modelo->info();
    unset($infoTable[0]);
    $dato = null;
    $disabled = null;

    if ($_GET['option'] != 'agregar'){//EDITAR-ELIMINAR
      $dato = $this->modelo->traerDato($_GET['id']);

      $values = [[
        'type'=>"hidden",
        'default'=>$dato[0]['ID'],
        'required'=>TRUE,
      ]];
      unset($dato[0]['ID']);

      $disabled = ($_GET['option'] == 'eliminar') ? TRUE : FALSE;

    } else {//Agregar
      unset($nombreColumnas[0]);
      $values = [];
    }

    foreach ($infoTable as $key) {
        array_push($values, [
          'label'=> $key['Field'],
          'default' => $dato[0][$key['Field']],
          'type'=>"text",
          'disabled' => $disabled,
          'required'=> ($key['Null'] == 'YES') ? FALSE: TRUE,
        ]);
      }

    $campos = array_combine($nombreColumnas, $values);
    $campos['ruta'] = self::$pluginpath;

    return $this->html->formulario($campos, $this->nombreTabla, "opcionesRegistro");
  }

  # =================================================
  # =        FUNCIONES AJAX PARA UN REGISTRO        =
  # =================================================

  function opcionesRegistro(){
    $datos = $_POST['datos']??NULL;
    $nombreTabla = $_POST['nTT']??NULL;
    $ruta = $_POST['ruta']??NULL;

    if (class_exists("controller")) {
      if (!isset($controlador)) {
        $controlador = new controller($nombreTabla, $ruta);
        if (isset($datos)) {
          switch ($_POST['option']) {
            case 'agregar':
                $controlador->modelo->salvar($datos) OR die("ERROR AL GUARDAR");
                die("Registro agregado \n".print_r($datos, true));
              break;
            case 'editar':
                $controlador->modelo->actualizar($datos, ["id"=>$datos["id"]]) OR die("ERROR AL ACTUALIZAR");
                die("Registro actualizado \n".print_r($datos, true));
              break;
            case 'eliminar':
                $controlador->modelo->borrar($datos[id]) OR die("ERROR AL ELIMINAR");
                die("Registro eliminado \n".print_r($datos, true));
              break;
          }
        }
      }
    }
    die("No llego el usuario");
  }


  //Pruebas-------------------------------------------------

  //modificar para enviar datos
  static function accionesModelo($modelo, $actions = ['opcion' => 'listar']){
    ?>
      <script type="text/javascript">
        jQuery(document).ready(function($) {
            ajaxMV("<?=$modelo?>", <?=json_encode($actions)?>);
        });
      </script>
    <?php
  }






  function crearArhivoModelo(){

	  $nombreModelo = strtolower($_GET['name']);

    if(!file_exists(RUTA.'/app/models/'.$nombreModelo.'.php')){

      $contenido = "<?php
      //Modelo ".ucfirst($nombreModelo)."
      class ".$nombreModelo." extends model{

      }";

      if($archivo = fopen(RUTA.'/app/models/'.$nombreModelo.'.php', "a")){
        if(!fwrite($archivo, $contenido."\n")){
          echo '<script type="text/javascript">
            alert("Ha habido un problema al crear el archivo");
          </script>';
          $this->accionesModelo(str_replace("cvip_","",$_REQUEST['page']), "listarModelos");
        }
      fclose($archivo);
      }

      $this->accionesModelo($nombreModelo);
    } else {
      echo '<script type="text/javascript">
        alert("El modelo ya existe");
      </script>';
      $this->accionesModelo(str_replace("cvip_","",$_REQUEST['page']), 'listarModelos');
    }
  }





}
