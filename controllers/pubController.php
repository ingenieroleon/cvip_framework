<?php

class pubController
{

  public $modelo;
  public $modelos;
  public $nombreModelo;
  public $nombreTabla;
  public $nombreColumnas;
  public $html;
  public $keyPrimary;
  public static $pluginpath;

  function __construct($modelo=NULL, $pluginpath = NULL){

     self::$pluginpath = $pluginpath;

     if ( $modelo ) {
      $this->nombremodelo = $modelo;
      $this->modelo = $this->modelo($modelo);
      $this->nombreTabla = $this->modelo->nombreTabla();
      $this->nombreColumnas = $this->modelo->nombreColumnas();
      $this->keyPrimary = $this->modelo->primaryKey;
    }

    //($this->keyPrimary)??$this->keyPrimary
    $this->html = new cvip_html( ($this->keyPrimary)??$this->keyPrimary );

  }

  # = RETORNA OBJETO DEL MODELO =
  function modelo($modelo){

    $rutaModelo = self::$pluginpath."/app/models/$modelo.php";

    if (file_exists($rutaModelo)) {
      require_once $rutaModelo;
      if (class_exists($modelo)) {
        return new $modelo();
      }
    }

    //OPCION PARA CREAR MODELO
    //Mostrar Mensaje en la consola
    //die("NO EXISTE EL MODELO");
  }

  public static function vista($nombreVista = '', $datos = array()){
      extract($_REQUEST);
      $rutaArchivo = self::$pluginpath."/app/views/public/$nombreVista.php";
      $rutaArchivoFW = RUTA_CVIP_FRAMEWORK."/views/public/$nombreVista.php";

      if (is_array($datos)){
        extract($datos);
      }

      ob_start();
      if (file_exists("$rutaArchivo")){
        require("$rutaArchivo");
      }
      elseif (file_exists($rutaArchivoFW)){
        require($rutaArchivoFW);
      }
      else {
        echo("<br>Error: No existe la vista: ".$nombreVista." <br>");
      }
      return ob_get_clean();
  }



  # = FUNCION POR DEFECTO, TRAE LOS DATOS DE LA BASE DE DATOS Y LOS LISTA =
  function pubListar(){

    //MOVER AL CONSTRUCTOR
    $vista = $this->html->crearTabla([
      "tabla"    => $this->nombreTabla,
      "datos" => $this->modelo->DB('ARRAY_A'),
      "buscador" => TRUE,
      "tamano" => '25',
      "modificacion" => FALSE,
    ]);
    //
    echo $this->vista('listar', $vista);
  }

}
