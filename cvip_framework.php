<?php
/*
 * Plugin Name: CVIP Framework
 * Plugin URI: ColombiaVIP.com
 * Description: Framework para sistemas.
 * Version: 20190513
 * Author: ColombiaVIP.com
 * Author URI: ColombiaVIP.com
 * License: PRIVADA
 */

defined ('ABSPATH') or die ("Ey!, no deberias estar aqui");

define("RUTA_CVIP_FRAMEWORK", dirname(__FILE__));//RUTA EN SERVIDOR DEL PLUGIN
//define('REQUEST_METHOD', $_SERVER['REQUEST_METHOD']);

class cvip_framework {
  public $nombreArchivo;
  //public $ruta = __DIR__;
  public $url;
  public $ver; //VERSION
  //public $nombrePlugin;
  public $opcion;

  public $pluginpath = null;
  public $nombrePlugin = null;
  public $modelos = null;

  //PRUEBAS ROUTER
  public $router;
  //PRUEBAS ROUTER


  function __construct($pluginpath) {
      # =================================================
      # = HTML HELPER, CONTROLADOR Y MODELO PRINCIPALES =
      # =================================================
      require_once 'html.php';

      require_once 'controllers/controller.php';
      require_once 'controllers/pubController.php';

      require_once 'model.php';
      require_once 'usuarios.php';
      require_once 'WP_Route.php';

      //NOMBRE DEL ARCHIVO CON RUTA
      $this->nombreArchivo = __FILE__;
      //RUTA PLUGIN EXTERNO
      $this->pluginpath = $pluginpath;
      //NOMBRE PLUGIN EXTERNO
      $this->nombrePlugin = basename($this->pluginpath);
      //VERSION
      $this->ver = time();

      $this->modelos = $this->obtenerModelos();


      # =============================================
      # =           HOOKS NECESARIOS DE WP        	=
      # =============================================
      add_action('wp_ajax_opcionesRegistro', array('controller', 'opcionesRegistro'), 10 );
      add_action('wp_ajax_nopriv_opcionesRegistro', array('controller', 'opcionesRegistro'), 10 );

      add_action('wp_ajax_cvip_ajax_mvc', array($this, 'cvip_ajax_mvc'), 10 );
      add_action('wp_ajax_nopriv_cvip_ajax_mvc', array($this, 'cvip_ajax_mvc'), 10 );

      add_shortcode( 'cvip_mvc' , array($this, 'shortcode_cvip_mvc'));

      // //EN PRUEBA
       add_action('wp_ajax_cargarControladoresAjax', array($this, 'cargarControladoresAjax'), 10 );
       add_action('wp_ajax_cargarControladoresAjax', array($this, 'cargarControladoresAjax'), 10 );


      # =========================================
      # = ADMIN MENU PRINCIPAL Y SUB PAGE MENU	=
      # =========================================
      # = ITEM PRINCIPAL (ADMIN MENU PAGE) =
      add_action('admin_menu', array($this, 'register_menu_page'));
      # = ITEMS HIJOS (ADMIN SUB MENU PAGE) =
      add_action('admin_menu', array($this, 'register_sub_menu_page'));


      # ===============================
      # = CSS Y SCRIPTS DEL FRAMEWORK	=
      # ===============================
      # = ARCHIVOS JS Y CSS =
      add_action('wp_enqueue_scripts', array($this, 'cargarEstilosScripts'));
      # = ARCHIVOS JS Y CSS ADMIN =
      add_action('admin_enqueue_scripts', array($this, 'cargarEstilosScripts'));


      # ===========================
      # = CSS Y SCRIPTS EXTERNOS	=
      # ============================
      # = ARCHIVOS JS Y CSS =
      add_action('wp_enqueue_scripts', array($this, 'cargarEstilosScriptsExternos'));
      // # = ARCHIVOS JS Y CSS ADMIN =
      add_action('admin_enqueue_scripts', array($this, 'cargarEstilosScriptsExternos'));

      # ==============================================
      # =              HOOK-PLUGIN                   =
      # = ACTIVACION, DESACTIVACION Y DESINSTALACION =
      # ==============================================
      # = ACTIVACION DEL PLUGIN  =
      register_activation_hook(RUTA_CVIP_FRAMEWORK.'/cvip_framework.php', array($this, 'cvip_activate'));
      # = DESACTIVACION DEL PLUGIN =
      register_deactivation_hook(RUTA_CVIP_FRAMEWORK.'/cvip_framework.php', array($this, 'cvip_deactivate'));

      # = DESINSTALACION DEL PLUGIN =
      //register_uninstall_hook(__FILE__, array($this, 'uninstall'));


      # ========================================
      # =              USUARIOS                =
      # ========================================
      # = NUEVOS CAMPOS AL USUARIO =
      add_action( 'show_user_profile', array($this, 'cvip_add_custom_user_fields') );
      add_action( 'edit_user_profile', array($this, 'cvip_add_custom_user_fields') );
      add_action( 'user_new_form', array($this, 'cvip_add_custom_user_fields') );

      # = GUARDAR DATOS =
      add_action( 'personal_options_update', array($this, 'cvip_save_custom_user_fields') );
      add_action( 'edit_user_profile_update', array($this, 'cvip_save_custom_user_fields') );

      # = Permisos =
      # = BLOQUEO ACCESO AL PERFIL =
      add_action('after_setup_theme', array($this, 'remove_admin_bar') );
      add_action( 'admin_menu', array($this, 'cvip_gl_capacidades') );


      //PRUEBAS ROUTER
      WP_Route::get('{modelo}/{opcion}', array($this, 'loadPubController') );
      // WP_Route::get ( 'vuelos/{flight}' , array($this, 'aviones') );
      //PRUEBAS ROUTER
  }


public static function loadPubController($modelo, $opcion, $pluginpath = NULL){

  if (!is_admin()){
    if (model::validarTabla(strtolower($modelo))){

      $rutaExterna = ($pluginpath) ? $pluginpath : $this->pluginpath;

      $pubControlador = $modelo.'Controller';
      $rutaPubControlador = $rutaExterna."/app/controllers/public/$pubControlador.php";

      if ( file_exists($rutaPubControlador) && class_exists($pubControlador) ){
        require_once $rutaPubControlador;
        $pubControlador = new $pubControlador($modelo, $rutaExterna);
      }else {
        $pubControlador = new pubController($modelo, $rutaExterna);
      }

      if (method_exists($pubControlador, $opcion)){
         $pubControlador->$opcion();
      }else{
        $pubControlador->pubListar();
      }
    }else {
      echo "ERROR: El modelo no existe";
    }
  }
}




# = SE CREAN LOS ROLES (USUARIOS) AL ACTIVAR EL PLUGIN =
function cvip_gl_add_role() {

  $usuarioPrincipal = strtoupper($this->nombrePlugin);

  //SE AGREGA EL ROL AL PRINCIPAL
  add_role($usuarioPrincipal, $usuarioPrincipal, array('read' => true, 'read_private_pages' => true));
  //LE AGREGO EL ROL AL ADMIN
  $GLOBALS['wp_roles']->add_cap('administrator', $usuarioPrincipal);
  //AGREGO EL ROL A TODOS LOS USUARIOS

  foreach ($this->modelos as $usuario) {
    add_role($usuario, $usuario, array('read' => true,
    'read_private_pages' => true,
     $usuarioPrincipal => true,
     'delete_posts' => true,
     'manage_own_attachments' => true,
     'edit_posts' => true,
     'upload_files' => true));

    $GLOBALS['wp_roles']->add_cap('administrator', $usuario);
    $GLOBALS['wp_roles']->add_cap($usuarioPrincipal, $usuario);

  }

}

# = SE REMUEVEN LOS ROLES (USUARIOS) AL DESACTIVAR EL PLUGIN =
function cvip_gl_remove_role() {
  remove_role(strtoupper($this->nombrePlugin));
  foreach ($this->modelos as $usuario) {
    remove_role($usuario);
  }
 }

 # = SE AGREGAN LOS CAMPOS AL USUARIO =
 function cvip_add_custom_user_fields( $user ) {
  $datos['user']=$user;//Para poder consultar el id del usuario y su meta
  echo controller::vista('datosUsuarios', $datos);
 }

 # =  =
 function cvip_save_custom_user_fields( $user_id ) {
   if ( !current_user_can( 'edit_user', $user_id ) ) {
       return false;
   }
   foreach ($_POST as $key => $value) {
         //Sanitize and validate the data
         update_user_meta( $user_id, "{$key}", sanitize_text_field($value) );
   }
 }



# = PERMISOS =
# = BLOQUEAR ACCESO AL PERFIL =
 function remove_admin_bar() {
 	if (!current_user_can('activate_plugins')) {
 		global $pagenow;
 		show_admin_bar(false);
 	}
 }


 function cvip_gl_capacidades(){
 	//MATA EL PROCESO SI NO ES ADMIN
 	if(!current_user_can('activate_plugins')) {
 		global $pagenow;

 		/*if ($pagenow == 'index.php' OR $pagenow == 'profile.php') {
 			wp_die( 'Por favor contacta a tu admin para solicitar acceso adicional.' );
    }*/
 	}
 }

  # ===============================================
  # = FUNCION PARA CARGAR TODOS LOS CSS Y SCRIPTS =
  # =           PROPIOS DEL FRAMEWORK             =
  # ===============================================
  function cargarEstilosScripts(){
    # = TRAE TODOS LOS ESTILOS Y SCRIPTS DEL FRAMEWORK =
    $this->registrarEstilos(glob(RUTA_CVIP_FRAMEWORK.'/assets/css/*.css'));
    $this->registrarScripts(glob(RUTA_CVIP_FRAMEWORK.'/assets/js/*.js'));
  }

  # = REGISTRA LOS ESTILOS =
  function registrarEstilos ($listado, $ruta = NULL){

    $ruta = ($ruta) ? $ruta : RUTA_CVIP_FRAMEWORK;

    foreach($listado as $estilo){
      $estilo = basename($estilo);
      wp_register_style($estilo, plugins_url(basename( $ruta )).'/assets/css/'.$estilo, array(), $this->ver);
      wp_enqueue_style($estilo);
    }
  }

  # = REGISTRA LOS SCRIPTS =
  function registrarScripts ($listado, $ruta =  NULL){

    $ruta = ($ruta) ? $ruta : RUTA_CVIP_FRAMEWORK;

    foreach($listado as $script){
      $script = basename($script);
      wp_register_script($script, plugins_url(basename( $ruta )).'/assets/js/'.$script, array("jquery"), $this->ver, TRUE);
      wp_enqueue_script($script);
      wp_localize_script($script, 'WP_AJAX', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
    }
  }

  # ===============================================
  # = FUNCION PARA CARGAR TODOS LOS CSS Y SCRIPTS =
  # =                  EXTERNOS                   =
  # ===============================================
  function cargarEstilosScriptsExternos(){
    # = TRAE TODOS LOS ESTILOS Y SCRIPTS DEL PLUGIN EXTERNO =
    $this->registrarEstilos( glob($this->pluginpath.'/assets/css/*.css'), $this->pluginpath);
    $this->registrarScripts( glob($this->pluginpath.'/assets/js/*.js'), $this->pluginpath);
  }

  # = REGISTRA LOS ESTILOS =
  // function registrarEstilos ($listado){
  //   foreach($listado as $estilo){
  //     $estilo = basename($estilo);
  //     wp_register_style($estilo, plugins_url(basename( RUTA_CVIP_FRAMEWORK )).'/assets/css/'.$estilo, array(), $this->ver);
  //     wp_enqueue_style($estilo);
  //   }
  // }
  //
  // # = REGISTRA LOS SCRIPTS =
  // function registrarScripts ($listado){
  //   foreach($listado as $script){
  //     $script = basename($script);
  //     wp_register_script($script, plugins_url(basename( RUTA_CVIP_FRAMEWORK )).'/assets/js/'.$script, array("jquery"), $this->ver, TRUE);
  //     wp_enqueue_script($script);
  //     wp_localize_script($script, 'WP_AJAX', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
  //   }
  // }





  # =================================================
  # = FUNCIONES PARA CARGAR MODELOS Y CONTROLADORES =
  # =================================================

  function register_menu_page(){
    // add_menu_page($this->nombrePlugin, ucfirst($this->nombrePlugin), strtoupper($this->nombrePlugin), $this->nombrePlugin, array($this, 'pagePrincipal'), null, 6);
    add_menu_page($this->nombrePlugin, ucfirst($this->nombrePlugin), 'administrator', $this->nombrePlugin, array($this, 'pagePrincipal'), null, 6);

    // add_menu_page($this->nombrePlugin, ucfirst($this->nombrePlugin), "administrator", $this->nombrePlugin, array($this, 'pagePrincipal'), null, 6);
  }

  # = TRAE ARRAY CON TODAS LAS RUTAS DE LOS MODELOS =
  function obtenerModelos(){
    $modelos = glob($this->pluginpath.'/app/models/*.php');

    $listadoModelos = [];
    foreach ($modelos as $modelo) {
      $listadoModelos[] = basename($modelo, '.php');
    }

    return $listadoModelos;
  }

  # ====================================================
  # = SE CREAN LAS SUB_PAGE CON LOS MODELOS EXISTENTES =
  # ====================================================
  function register_sub_menu_page(){
    foreach ($this->modelos as $modelo) {
      $modelo = basename($modelo, '.php');
      add_submenu_page( $this->nombrePlugin, ucfirst($modelo), $modelo, $modelo,  $modelo, array($this, 'controladoresOpciones'));
    }
    //SE REMUEVE EL HIJO PARA QUE NO QUEDE DUPLICADO AL PADRE
    //remove_submenu_page( $this->nombrePlugin, $this->nombrePlugin );
  }

  //METODO PARA LA PAGINA PRINCIPAL
  function pagePrincipal(){
    $this->loadController('', 'principal');
  }

  # ======================================================================
  # =                             IMPORTANTE                             =
  # =     NO SE LLAMA DIRECTAMENTE A LA FUNCION 'cargarControlador'      =
  # = PARA EVITAR QUE EN EL FRONTEND NO SE FILTRE INFORMACION INDESEADA  =
  # ======================================================================
  function controladoresOpciones(){
    $modelo = $_REQUEST['page'];
    $opcion = $_REQUEST["option"]??"listar";

    $this->loadController($modelo, $opcion);
  }

  # ==================================================
  # = SHORCODE PARA HACER UNA SOLICITUD AL FRAMEWORK =
  # ==================================================
  function shortcode_cvip_mvc($atts) {
    $atts = shortcode_atts( array(
      'c' => '',
      'o' => 'listar',
      'plugin' => ''
    ), $atts);

    $modelo = $atts["c"];
    $opcion = $atts["o"];
    $plugin = $atts["plugin"];

    $this->preparacion($modelo, $opcion, $plugin);
  }

function preparacion($modelo, $opcion, $plugin){
  $plugin = plugin_dir_path( __DIR__ ).$plugin;
  $this->loadPubController($modelo, $opcion, $plugin);
}


  # =============================================================================
  # =       FUNCION ENCARGADA DEL DIRECCIONAMIENTO DEPENDIENDO LOS METODOS      =
  # = CARGA EL CONTROLADOR CORRESPONDIENTE AL MODELO O POR DEFECTO EL PRINCIPAL =
  # =============================================================================
  function loadController($modelo, $opcion, $plugin = NULL){

    if (model::validarTabla(strtolower($modelo))){
      $rutaExterna = ($plugin) ? $plugin: $this->pluginpath;

      $controlador = $modelo."Controller";
      $rutaControlador = $rutaExterna."/app/controllers/$controlador.php";

      if (file_exists($rutaControlador)){
        require_once $rutaControlador;
        $controlador = new $controlador($modelo, $rutaExterna);
      }else {
        $controlador = new controller($modelo, $rutaExterna);
      }

      if (method_exists($controlador, $opcion)){
         $controlador->$opcion();
      }else{
        $controlador->listar();
      }

    }else {
      echo "ERROR: El modelo no existe";
    }
  }

  # ==============================================
  # =            FUNCIONES EN PRUEBA             =
  # ==============================================

  function cargarControladoresAjax() {

    $opcionesArray = $_POST;
    array_shift($opcionesArray);
    array_shift($opcionesArray);

    $enlace = '';

    foreach ($opcionesArray as $opcion => $value) {
      $enlace .= '&'.$opcion.'='.$value;
    }

    die(admin_url().URL_MODELO.'cvip_'.strtolower($_POST['modelo']).$enlace);
  }

  //----------------------------

  function cvip_ajax_mvc() {//controlador-
    //$mvc=empty($_REQUEST["mvc"])?NULL:$_REQUEST["mvc"];
    $mvc = $_REQUEST;
    loadController($modelo, $opcion);
    die(controller::vista('editar', $mvc));// never forget to die() your AJAX reuqests
  }

  # ==============================================
  # =             FUNCIONES-PLUGIN               =
  # =      ACTIVAR, DESACTIVAR Y DESINTALAR      =
  # ==============================================

  function cvip_activate(){

    // require 'activacion.php';

    # ============================================
    # = SE CREAN LOS ROLES BASADO EN LOS MODELOS =
    # =           AL ACTIVAR EL PLUGIN           =
    # ============================================
    $this->cvip_gl_add_role();

    flush_rewrite_rules();
  }

  function cvip_deactivate(){
    # ===============================================
    # = SE REMUEVEN LOS ROLES BASADO EN LOS MODELOS =
    # =           AL DESACTIVAR EL PLUGIN           =
    # ===============================================
    $this->cvip_gl_remove_role();

    flush_rewrite_rules();
  }

  static function uninstall(){

  }

}
