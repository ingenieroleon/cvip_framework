<?php
/**
 * HTML CLASS
 * Contiene los metodos para generar elementos HTML de forma dinamica.
 */

class cvip_html {

  public $keyPrimary;

  function __construct($keyPrimary = NULL){
    if ($keyPrimary){
      //$this->keyPrimary = 'ID';
      $this->keyPrimary = $keyPrimary;
    }
  }

  function crearTabla($array){

    ob_start();

    $tabla; //NOMBRE DE LA TABLA (NECESARIO PARA EDICIÓN)
    $limit='10000'; // LIMITE DE RESULTADOS POR CONSULTA (NO SE NECESITA SI SE PASA LA CONSULTA)
    $encabeza=TRUE; //SI MUESTRA TITULO DE COLUMNAS
    $buscador=FALSE; //ACITVA EL BUSCADOR
    $modificacion=FALSE; //PERMITE EDITAR
    $esconder=array(); //ARRAY DE COLUMNAS A ESCONDER
    $encoger=array(); //ARRAY DE COLUMNAS A ENCOGE EN MOVIL
    $omitir=array(); //ARRAY A OMITIR EN EDICIÓN ÚTIL CUANDO SE RENOMBRE LA COLUMNA EN EL QUERY
    $tamano = '25'; //CANTIDAD DE RESULTADOS POR PAGINA
    $datos=FALSE; //SE PASAN LOS DATOS
    $funcionAntes=FALSE; //Corre un script antes de mostrar la tabla
    extract($array);
    $nombretabla='tabla'.rand(1,100);//ES RANDOM PARA EVITAR CONFLICTOS SI SE CREAN MAS DE 2 TABLAS EN UNA WEB



    //ACCIONES TABLA
    //if(!empty($_GET["opcion"])) return cvip_framework::cargarControlador($tabla, $_GET["opcion"]);



    //Cuerpo de la tabla
    echo '<table class="'.$nombretabla.' tabla_'.$tabla.' mitabla" data-paging="true" data-paging-limit="10" data-paging-count-format="Pág. {CP} / {TP}" data-paging-size='.$tamano.' data-sorting="true" data-filtering="'.(($buscador)?'true':'false').'" data-filter-dropdown-title="Buscar en:" data-filter-placeholder="Buscar" data-cascade="true" data-use-parent-width="true" data-empty="No hay registros">';

    if (isset($datos[0])) {/*MIRA SI HAY REGISTROS*/
        if ($encabeza){/*MIRA SI QUIERE PONER ENCABEZADOS*/
          $campos=$datos[0];
          ?>
          <thead><tr>

          <?php
          foreach($campos as $campo => $valor){
            if (!in_array($campo,$esconder)) echo '<th '.((in_array($campo,$encoger))?'data-breakpoints="md"':"").'>'.$campo.'</th>';//ESCONDO EL CAMPO
            else echo '<th data-visible="false" data-sortable="false" data-filterable="false">'.$campo.'</th>';
          }
          if ($modificacion) echo '<th data-breakpoints="md" data-name="ACCIONES" data-sortable="false" data-filterable="false">ACCIONES</th></tr></thead>';
        }

    echo '<tbody class="input-filter">';
          foreach($datos as $registro){

            echo '<tr>';
            foreach($registro as $campo => $valor){
              echo "<td class=".sanitize_title($campo).">".$valor."</td>";//EL CAMPO ESTA ESCONDIDO GRACIAS AL HEADER
              if (in_array($campo,$omitir)) unset($registro["$campo"]);//OMITO EL CAMPO PARA LA CONSULTA DE LA EDICIÓN
            }

            if ($modificacion){
              //IMPORTANTE, VER COMO TRAER EL ID, TENIENDO EN CUENTA QUE PUEDE VARIAR

              $editar='<a href="?'.http_build_query($_GET).'&amp;option=editar&amp;id='.$registro["$this->keyPrimary"].'"><span class="glyphicon glyphicon-pencil" title="EDITAR" style="font-size:25px;">E</span></a>';
              $eliminar='<a href="?'.http_build_query($_GET).'&amp;option=eliminar&amp;id='.$registro["$this->keyPrimary"].'"><span class="glyphicon glyphicon-remove" title="ELIMINAR" style="color:#dd3333; font-size:25px;">X</span></a>';

              //$editar = '<a href="'.URL_MODELO.'&opcion=editar&id='.$registro->id.'"><span class="glyphicon glyphicon-pencil" title="EDITAR" style="font-size:25px;">E</span> </a>';
              //$eliminar = '<a href="'.URL_MODELO.'&opcion=eliminar&id='.$registro->id.'"><span class="glyphicon glyphicon-pencil" title="ELIMINAR" style="color:#dd3333; font-size:25px;">X</span></a>';

              echo "<td class=acciones>".$editar.' <> '.$eliminar."</td></tr>";
            }
          }
    echo '</tbody>';
        }
      /*SI NO HAY REGISTROS*/
      else
        echo '<td>no hay registros para mostrar</td>';

      echo '</table>';//FIN DE LA TABLA

      ?>
        <script type='text/javascript'>
          <?php if ($funcionAntes){
            echo "{$funcionAntes}";
          }
          ?>

          jQuery(function () {
            jQuery('.<?= $nombretabla ?>').footable();
          });

          //BOTON AGREGAR (EN PRUEBA)
          jQuery(function(){
            //Solo lo toma el backend
            jQuery(".<?=$nombretabla?> .form-inline").prepend("<div class='" + 'button-agregar page-principal ' + "<?=$_GET['page']?>" + "'> <input onclick="+"window.location.href='"+"admin.php?"+"page=<?=$_GET['page']?>&opcion=agregar&tabla=<?=$tabla?>'"+" type="+"button"+" value="+"Agregar"+"> </div>");
          });

        </script>
      <?php
    return ob_get_clean();
  }

  //FUNCION GENERA UN FORM AUTOMATICO CON TODOS SUS CAMPOS
  public function formulario($campos, $nombreTabla=NULL, $clases=NULL){//PENSAR COMO CONVERTIR ESTA Y OTRAS FUNCIONES PARA QUE SIEMRPE RECIBAN UN ARRAY DE ARGUMENTOS

    $ruta = $campos['ruta'];
    array_pop($campos);
    ob_start();

    ?>
    <div class="contenedor<?=$nombreTabla??"Form" ?>">

      <!-- BOTON VOLVER -->
      <div class="button-volver">
        <a href="<?=$_SERVER['HTTP_REFERER'] ?>">
          <input type="button" value="&laquo;Volver">
        </a>
      </div>
      <!--  ___________________________  -->

      <form class="<?=$clases ?> <?=$nombreTabla??'asyn'?>" id="<?=$nombreTabla?>" onsubmit="event.preventDefault()">

        <?php foreach ($campos as $key=>$campo): ?>
          <div class="campo <?=$key?> <?=@$campo['class']?>">
            <?php if (ISSET($campo['label'])): ?>
              <label for="datos[<?= $key ?>]"><?=@$campo['label']?></label>
            <?php endif; ?>
            <?php switch ($campo['type']) {
              case 'select':
              echo cvip_html::select($key, $campo);
                break;

              case 'textarea':
              echo cvip_html::textarea($key, $campo);
                break;

              case 'media':
              echo cvip_html::media($key, $campo);
                break;

              default:
              echo cvip_html::input($key, $campo);
                break;
            } ?>
          </div>
        <?php endforeach; ?>

        <?php //NOMBRE TABLA EN CODIGO PARA EVITAR PROBLEMAS ?>
        <?php if ($nombreTabla): ?>
          <input type="hidden" name="nTT" value="<?=$nombreTabla?>">
        <?php endif; ?>

        <input type="hidden" name="option" value="<?=$_GET['option']?>">
        <input type="hidden" name="ruta" value="<?=$ruta ?>">

        <div class="submit">
          <input type="submit" name="ENVIAR" value="ENVIAR">
        </div>

      </form>
    </div>
    <?php
    return ob_get_clean();
  }

  function select($key, $campo){
    ob_start();
    ?>
    <select name="datos[<?= $key ?>]"
      <?=(isset($campo['required']) AND ($campo['required'])) ?"required":"" ?>
      >
      <?php if (!isset($campo['default']) OR $campo['default']==""): ?>
        <option value="" selected disabled><?=$campo['placeholder']??"Click &#x25BC;"?></option>
      <?php endif; ?>

      <?php foreach ($campo['options'] as $opcionk => $opcionv): ?>
        <option value="<?=$opcionk?>"
          <?=(isset($campo['default'])AND($campo['default']==$opcionk)) ?"selected":"" ?>>
          <?=$opcionv?>
        </option>
      <?php endforeach; ?>
    </select>
    <?php
    return ob_get_clean();
  }

  function textarea($key, $campo){
    ob_start();
    ?>
    <textarea name="datos[<?= $key ?>]" rows="8" cols="80"
      <?=(isset($campo['required']) AND ($campo['required'])) ?"required":"" ?>
      placeholder="<?=@$campo['placeholder']?>"
      <?=(isset($campo['disabled']) AND ($campo['disabled'])) ?"disabled":"" ?>
    ></textarea>
    <?php
    return ob_get_clean();
  }

  function input($key, $campo){
    ob_start();
    ?>
    <input
    type="<?=$campo['type']??"text"?>"
    <?php if ($key!="mvc"): ?>
      name="datos[<?= $key ?>]"
    <?php else: ?>
      name="<?= $key ?>"
    <?php endif; ?>

    value="<?=@$campo['default']?>"
    placeholder="<?=@$campo['placeholder']?>"
    <?=(isset($campo['required']) AND ($campo['required'])) ?"required":"" ?>
    <?=(isset($campo['disabled']) AND ($campo['disabled'])) ?"disabled":"" ?>
    >
    <?php
    return ob_get_clean();
  }

  function media($key, $campo){
    wp_enqueue_media();
    ob_start();
    ?>
    <div class="mediaUpload">
      <input
      type="text"class="myfile"
      <?php if ($key!="mvc"): ?>
        name="datos[<?= $key ?>]"
      <?php else: ?>
        name="<?= $key ?>"
      <?php endif; ?>

      value="<?=@$campo['default']?>"
      placeholder="<?=@$campo['placeholder']?>"
      <?=(isset($campo['required']) AND ($campo['required'])) ?"required":"" ?>
      >

      <button class="upload upload_<?=$key?>">SUBIR</button>
      <button class="remove remove_<?=$key?>">&times;</button>
      <script type="text/javascript" style="display:none">
      jQuery(document).ready(function($) {
        // The "Upload" button
        $('.upload_<?=$key?>').click(function() {
          var button = $(this);
          wp.media.editor.send.attachment = function(props, attachment) {
            var websiteName = window.location.protocol+"//"+window.location.host;
            var relativeUrl = attachment.url.replace(websiteName, "");
            console.log(websiteName);
              $('.<?=$key?> input').val(relativeUrl);
          }
          wp.media.editor.open(button);
          return false;
        });

        $('.remove_<?=$key?>').click(function() {
          var answer = confirm('Seguro?');
          if (answer == true) {
            $('.<?=$key?> input').val("");
          }
          return false;
        });

      });
      </script>
    </div>
    <?php
    return ob_get_clean();
  }

  function fecha(){
    date_default_timezone_set('America/Bogota');
    return date("Y-m-d H:i:s");
  }

  //¿Lugar correcto para esta funcion?
  static function permisos($listado=FALSE){

    foreach ($listado as $key => $value) {
       if (current_user_can($value)) {
         return;
       }
    }

    die("No tienes permiso para estar en esta area.");
  }

}


 ?>
