<?php
/**
 * MODELO PRINCIPAL
 */

//CREAR FUNCION QUE RECIBA UN MENSAJE DE ERROR Y LO MUESTRE, YA SE POR HTML O COMO ALERT (O DEJAR ENVIAR POR QUE MEDIO MOSTRAR HTML-ALERT-CONSOLE)

class model {

  public $nombreTabla;
  public $query;
  public $wpdb;

  //EN PRUEBAS
  public $primaryKey = null;
  public $campos = null;

  public $belongsTo = null;
  public $hasMany = null;

  //EN PRUEBAS


    function __construct($nombreTabla = NULL){
      global $wpdb;
      // $html = new html();
      if(!$this->nombreTabla=$nombreTabla)

        $this->nombreTabla=self::nombreTabla();
        $this->wpdb=$wpdb;

        $this->primaryKey = $this->primarykey();

        // if ($this->campos){
        //   $this->campos = $this->converCampos($this->campos);
        // }

        $this->query = $this->crearQueryPrueba();

    }

    //Retorna el resultado del main query
    function DB($tipo="OBJECT")
    {
      return $this->wpdb->get_results($this->query,$tipo);
    }

    //Retorna un array de opciones listo para usar en un select
    function opciones(){

      foreach ($this->DB("ARRAY_N") as $opcion) {
        $opciones[$opcion[0]]=$opcion[1];
      }
      return $opciones;
    }

    //Retorna el nombre de la tabla basado en el nombre del modelo
    public static function nombreTabla() {
      return strtolower(get_called_class());
    }

    //Retorna un array con el nombre de las columnas
    public static function nombreColumnas(){
      global $wpdb;
      $query="SELECT column_name
      from information_schema.columns
      where table_name = '".self::nombreTabla()."';";
      return $wpdb->get_col($query);
    }

  //Convierte un array de columnas a un string para SQL
    public static function columnaString ( $arrayColumnas ) {
      if ( empty( $arrayColumnas ) )
        return '*';
      if ( is_string( $arrayColumnas ) )
        return $arrayColumnas;
      if ( is_array( $arrayColumnas ) )
        return implode( ', ', $arrayColumnas );
    }

    //Retorna el valor del ultimo id (No usar en tablas sin ID)
    public static function ultimoId() {
      global $wpdb;
      $query='SELECT MAX(id) FROM '.self::nombreTabla();
      return $wpdb->get_var($query);
    }

    //Recibe un nombre de columna y devuelve su valor basado en el id (No usar en tablas sin ID)
    public static function ultimo($columna) {
      global $wpdb;
      $query="SELECT $columna FROM ".self::nombreTabla()." WHERE id = ".self::ultimoId();
      return $wpdb->get_var($query);
    }

    //Recibe un array de columnas y devuelve el valor de la ultima fila dentro de un array (No usar en tablas sin ID)
    public static function ultimos($columnas="*") {
      global $wpdb;
      $columnas=self::columnaString($columnas);
      $query="SELECT $columnas FROM ".self::nombreTabla()." WHERE id = ".self::ultimoId();
      return $wpdb->get_row($query, "ARRAY_A");
    }

    //Devuelve un array con los nombres de las columnas de la tabla y todos los valores en NULL
    public static function describir(){
      $nombreColumnas=self::nombreColumnas();
      foreach ($nombreColumnas as $key => $value) {
        $llenarNull[$value]=null;//SE CAMBIO ESTA LINEA PARA QUE EL NULL AGARRE BIEN
      }
      return $llenarNull;
    }

    //CUENTA LOS REGISTROS QUE CUMPLEN CON UNA CONDICIÓN
    function contar($where=NULL){
      $tabla=self::nombreTabla();
      $query="SELECT COUNT(*) FROM $tabla";
      if ( $where )
        $query .= ' WHERE ' . ( is_array( $where ) ? implode( ' AND ', $where ) : $where );
      return $this->wpdb->get_var($query);
    }

    //Mezcla el array recibido + el que describe la tabla para llenar de nulls las columans vacias
    function construir($fila){
      return array_merge(self::describir(), $fila);
    }

    # ==============================
    # =             CUD            =
    # ==============================

    //Guarda el registro construyendo la fila y salvandola en base de datos
    function salvar($fila){
      $filaLista=$this->construir($fila);
      $result=$this->wpdb->insert( self::nombreTabla(), $filaLista);
      // Print last SQL query string
      if (!$result) {
        echo "ERROR: <pre>".$this->wpdb->last_query."</pre>"; //SI HAY ERROR IMPRIME EL QUERY
      }
      // $this->insert_id=$wpdb->insert_id;
      return $result;
    }

    //Recorre el array recibido y salva cada fila
    function salvarVarios($filas){
      foreach ($filas as $key => $fila) {
        $result=$this->salvar($fila);
        if (!$result) {
          return $result; //SI HAY ERROR DETIENE EL CICLO
        }
      }
      return $result;
    }

    //ACTUALIZA UN REGISTRO DONDE SE DE UNA CONDICIÓN
    function actualizar($fila, $where){
      $result=$this->wpdb->update(self::nombreTabla(), $fila, $where);
      // Print last SQL query string
      if (!$result) {
        echo "ERROR: <pre>".$this->wpdb->last_query."</pre>"; //SI HAY ERROR IMPRIME EL QUERY
      }
      return $result;
    }

    function borrar ($id){
      $result = $this->wpdb->delete(self::nombreTabla(), array('ID' => $id));
      if (!$result) {
        echo "ERROR: <pre>".$this->wpdb->last_query."</pre>"; //SI HAY ERROR IMPRIME EL QUERY
      }
      return $result;
    }

    # =             CUD            =

    //FUNCION QUE RETORNA UN QUERY RECIBIENDO UNOS PARAMETROS
    function crearQuery($columnas=null, $where=null, $order=null, $limit=null){
      $columnas=self::columnaString($columnas);
      $tabla=self::nombreTabla();
      $query="SELECT $columnas FROM $tabla";
      if ( $where )
        $query .= ' WHERE ' . ( is_array( $where ) ? implode( ' AND ', $where ) : $where );
      if ( $order )
        $query .= ' ORDER BY ' . ( is_array( $order ) ? implode( ', ', $order ) : $order );
      if ( $limit )
        $query .= ' LIMIT ' . $limit;


      return $query;
    }


    //Selecciona con un array de columnas y un array de condiciones
    function seleccionar($columnas=null, $where=null, $order=null, $limit=null){
      $query = $this->crearQuery($columnas,$where,$order,$limit);
      return $this->wpdb->get_results($query);
    }


  //METODOS EN PRUEBAS

  //FUNCION QUE RETORNA UN QUERY RECIBIENDO UNOS PARAMETROS
  function crearQueryPrueba($columnas=null, $where=null, $order=null, $limit=null){

    $tabla = self::nombreTabla();
    $columnas = $this->columnaString($columnas);
    $leftJoins = null;
    $stringLeftJoins = '';
    $camposExternos = null;
    $stringCamposExternos = '';

    if ($this->campos){
      $columnas = $this->converAsCampos ($this->campos, $tabla);
      $columnas = implode( ', ', $columnas );
    }

    //UNIFICAR BELONGS TO Y HAS MANY EN UNA FUNCION QUE RECIBA LO NECESARIO
    if (is_array($this->belongsTo)){
      foreach ($this->belongsTo as $tablaExterna => $datos) {
        $stringCamposExternos .= ', ';
        $camposExternos = @(is_array($datos) && $datos['campos']) ? $this->converAsCampos($datos['campos'], $tablaExterna) : die("ERROR: Verifique los campos a traer.");//REEMPLAZAR POR LA FUNCION DE MOSTRAR ERRORES(FALTA)
        $leftJoins[] = "LEFT JOIN $tablaExterna ON $tabla.".$datos['keyExterna']." = $tablaExterna.id";
        $stringCamposExternos .= implode( ', ', $camposExternos );
      }
      $stringLeftJoins .= implode( ' ', $leftJoins );
      $columnas .= " $stringCamposExternos";
    }

    //CON ERRORES, ADAPTAR PARA LAS RELACIONES RIGHT JOIN
    /*
    $stringHasMany = '';
    $rightJoins = [];
    if (is_array($this->hasMany)){

      foreach ($this->hasMany as $tablaExterna => $datos) {
        $stringHasMany .= ', ';
        $camposExternos = @(is_array($datos) && $datos['campos']) ? $this->converAsCampos($datos['campos'], $tablaExterna) : die("ERROR: Verifique los campos a traer.");//REEMPLAZAR POR LA FUNCION DE MOSTRAR ERRORES(FALTA)

        $rightJoins[] = " RIGHT JOIN $tablaExterna ON $tablaExterna.".$datos['keyExterna']." = $tabla.ID";


        $stringHasMany .= implode( ', ', $camposExternos );
      }
      $right = implode( ' ', $rightJoins );

      $columnas .= " $stringHasMany";
    }
    */

    $query = "SELECT $columnas FROM $tabla ".$stringLeftJoins;

    if ( $where )
      $query .= ' WHERE ' . ( is_array( $where ) ? implode( ' AND ', $where ) : $where);
    if ( $order )
      $query .= ' ORDER BY ' . ( is_array( $order ) ? implode( ', ', $order ) : $order );
    if ( $limit )
      $query .= ' LIMIT ' . $limit;

    return $query;
  }


//FUNCCION HAS MANY
  function hasMany ($tabla, $datos, $id, $order = NULL, $limit = NULL){
    // SELECT term_taxonomy_id AS 'ID Categoria' FROM `wp_term_relationships` WHERE wp_term_relationships.object_id = 9;

    if ($datos['campos']){
      $columnas = $this->converAsCampos ($datos['campos'], $tabla);
      $columnas = implode( ', ', $columnas );
    }

    $query = "SELECT $columnas FROM $tabla WHERE $tabla.".$datos['keyExterna']."= $id";

    //if ( $where || !empty( $stringLeftJoins ) )
    // $query .= ' WHERE ' . ( is_array( $where ) ? implode( ' AND ', $where ) : $where);
    if ( $order )
      $query .= ' ORDER BY ' . ( is_array( $order ) ? implode( ', ', $order ) : $order );
    if ( $limit )
      $query .= ' LIMIT ' . $limit;

    return $this->wpdb->get_results($query, 'ARRAY_A');;
  }



  public function primarykey($tabla = null, $tipo = 'OBJECT'){
    global $wpdb;
    $tabla = ($tabla) ? $tabla : strtolower(get_called_class());
    $query = "SELECT * FROM information_schema.KEY_COLUMN_USAGE WHERE KEY_COLUMN_USAGE.TABLE_NAME  = '$tabla'";

    return $wpdb->get_results($query, $tipo)[0]->COLUMN_NAME;
  }



  public function converAsCampos ($campos, $tablaExterna){
    if ( empty( $campos ) )
      return '';
    if ( is_string( $campos ) )
      return $campos;
    if ( is_array( $campos ) ){

      $columnas = [];
      foreach ($campos as $key => $value) {
        $columnas [] = "$tablaExterna.$key AS '$value'";
      }
      //$columnas = implode( ', ', $columnas );

      return $columnas;
    }

  }





    function showIndex($tabla=null){
      global $wpdb;
      $query = "SHOW INDEX FROM ".$tabla;
      return $this->wpdb->get_results($query);
    }

    static function showFullTables($tipo="OBJECT"){
      global $wpdb;
      $query = "SHOW FULL TABLES FROM ".DB_NAME;
      return $wpdb->get_results($query, $tipo);
    }

    static function validarTabla($tabla, $tipo="ARRAY_N"){
      global $wpdb;
      $query = "SHOW TABLES LIKE '$tabla'";
      return $wpdb->get_results($query, $tipo);
    }

    function info($tipo = "ARRAY_A"){
      $tabla = self::nombreTabla();
      $query = "DESCRIBE ".$tabla;
      return $this->wpdb->get_results($query, $tipo );
    }

    //Selecciona un dato
    function traerDato($id, $tipo = 'ARRAY_A'){
      $query = "SELECT * FROM ".self::nombreTabla()." WHERE id = $id";
      return $this->wpdb->get_results($query, $tipo);
    }

}
