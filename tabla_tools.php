<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

function estilostabla(){

	//Funciones para registrar estilos y scripts automaticamente con el nombre
	function registrarestilos ($listado){ foreach($listado as $estilo)
		{wp_register_style($estilo, CVIP_URL.'/_CVIP_framework/css/'.$estilo.'.css'); wp_enqueue_style($estilo);} }
	function registrarscripts ($listado){ foreach($listado as $script)
		{wp_register_script($script, CVIP_URL.'/_CVIP_framework/js/'.$script.'.js'); wp_enqueue_script($script);} }

	//Resgitro todos los estilos y scripts
	registrarestilos(array('tabla_tools','footable.standalone.min'));
	registrarscripts(array('footable.min'));
}

add_action('wp_enqueue_scripts','estilostabla');
add_action('admin_enqueue_scripts','estilostabla');



//Auto crear Tabla
//tabla, Usar encabezados, buscador, modificacion si el usuario tiene el poder de editar campos, esconder campos, encoger campos, tamaño de las páginas, limit de la busqueda, consulta,  Omitir EN EDICION
function crear_tabla($data){
	ob_start();

	//POR SEGURIDAD TRANSMITO LA TABLA Y LAS COLUMNAS COMRPIMIDAS Y LAS DESCOMPRIMO AQUÍ
	if(!empty($_GET["sel"])){
		extract(json_decode(base64_decode($_GET["sel"]),true));
	}
	//AGREGAR, EDITAR Y ELIMINAR
	if(!empty($_GET["accion"]))
	switch ($_GET["accion"]) {
		case "AGREGAR":
		        /*Nombre de la tabla*/
				return agregar_Registro($_GET["tabla"]);
		        break;

		case "EDITAR":
		        /*Nombre de la tabla*/
				return editar_Registro($tabla,$esteregistro);
		        break;

		case "ELIMINAR":
		        /*Nombre de la tabla*/
				return editar_Registro($tabla,$esteregistro);
		        break;
	}

	$tabla; //NOMBRE DE LA TABLA (NECESARIO PARA EDICIÓN)
	$limit='10000'; // LIMITE DE RESULTADOS POR CONSULTA (NO SE NECESITA SI SE PASA LA CONSULTA)
	$encabeza=TRUE; //SI MUESTRA TITULO DE COLUMNAS
	$buscador=FALSE; //ACITVA EL BUSCADOR
	$modificacion=FALSE; //PERMITE EDITAR
	$esconder=array(); //ARRAY DE COLUMNAS A ESCONDER
	$encoger=array(); //ARRAY DE COLUMNAS A ENCOGE EN MOVIL
	$omitir=array(); //ARRAY A OMITIR EN EDICIÓN ÚTIL CUANDO SE RENOMBRE LA COLUMNA EN EL QUERY
	$tamano='10'; //CANTIDAD DE RESULTADOS POR PAGINA
	$consulta=FALSE; //SI SE PASA LA CONSULTA SE IGNORA LA TABLA Y EL LIMITE
	$funcionAntes=FALSE; //Corre un script antes de mostrar la tabla
	extract($data);
	$nombretabla='tabla'.rand(1,100);//ES RANDOM PARA EVITAR CONFLICTOS SI SE CREAN MAS DE 2 TABLAS EN UNA WEB


	global $wpdb;
	if (!$consulta) $consulta="SELECT * FROM $tabla ORDER BY 1 DESC LIMIT $limit";	$registros=$wpdb->get_results($consulta);

	$pagina="";
	if(!empty($_GET["page"]))
	$pagina="page=".$_GET["page"];

	if ($modificacion){//SI ACEPTA MODIFICACION BOTON AGREGAR
		echo '<p><a class="botonpeque" href="?'.http_build_query($_GET).'&amp;accion=AGREGAR&amp;tabla='.$tabla.'">AGREGAR</a></p>';
	}

	//Cuerpo de la tabla
	echo '<table class="'.$nombretabla.' tabla_'.$tabla.' mitabla" data-paging="true" data-paging-limit="10" data-paging-count-format="Pág. {CP} / {TP}" data-paging-size='.$tamano.' data-sorting="true" data-filtering="'.(($buscador)?'true':'false').'" data-filter-dropdown-title="Buscar en:" data-filter-placeholder="Buscar" data-cascade="true" data-use-parent-width="true" data-empty="No hay registros">';

	if (isset($registros[0]))		{/*MIRA SI HAY REGISTROS*/
			if ($encabeza){/*MIRA SI QUIERE PONER ENCABEZADOS*/
				$campos=$registros[0];
				echo '<thead><tr>';
				foreach($campos as $campo => $valor){
					if (!in_array($campo,$esconder)) echo '<th '.((in_array($campo,$encoger))?'data-breakpoints="md"':"").'>'.$campo.'</th>';//ESCONDO EL CAMPO
					else echo '<th data-visible="false" data-sortable="false" data-filterable="false">'.$campo.'</th>';
				}
				if ($modificacion) echo '<th data-breakpoints="md" data-name="ACCIONES" data-sortable="false" data-filterable="false">ACCIONES</th></tr></thead>';
			}
	echo '<tbody class="input-filter">';
				foreach($registros as $registro){
					echo '<tr>';
					foreach($registro as $campo => $valor){
						echo "<td class=".sanitize_title($campo).">".$valor."</td>";//EL CAMPO ESTA ESCONDIDO GRACIAS AL HEADER
						if (in_array($campo,$omitir)) unset($registro->$campo);//OMITO EL CAMPO PARA LA CONSULTA DE LA EDICIÓN
					}
					if ($modificacion){
						$esteregistro=(array) $registro;
						$seleccion=base64_encode(json_encode(compact("tabla","esteregistro")));

						$editar='<a href="?'.http_build_query($_GET).'&amp;accion=EDITAR&amp;sel='.$seleccion.'"><span class="glyphicon glyphicon-pencil" title="EDITAR" style="font-size:25px;">E</span></a>';
						$eliminar='<a href="?'.http_build_query($_GET).'&amp;accion=ELIMINAR&amp;sel='.$seleccion.'"><span class="glyphicon glyphicon-remove" title="ELIMINAR" style="color:#dd3333; font-size:25px;">X</span></a>';
						echo "<td class=acciones>".$editar.' <> '.$eliminar."</td></tr>";
					}
				}
	echo '</tbody>';
			}
		/*SI NO HAY REGISTROS*/
		else
			echo '<td>no hay registros para mostrar</td>';


		echo '</table>';//FIN DE LA TABLA



?>
	<script type='text/javascript'>
	<?php if ($funcionAntes){
		echo "{$funcionAntes}";
	}
	?>

	jQuery(function () {
		jQuery('.<?php echo $nombretabla ?>').footable();

	});


	</script>
<?php
return ob_get_clean();
}






function editar_registro($tabla,$esteregistro){
	global $wpdb;$wpdb->show_errors();
	$where=array();
	foreach ($esteregistro as $registro => $valor) {
		$valor=$wpdb->esc_like($valor);
		$where[]="$registro = '$valor'";
	}
	$where=implode('AND ',$where);

	$anteriores=$esteregistro;

	$consulta="SELECT * FROM $tabla WHERE $where";	$registros=$wpdb->get_results($consulta);
	//echo "<pre>";Print_r($registros);echo "</pre>";
	echo "<code>";Print_r($consulta);echo "</code>";

	$deshabilitar=($_GET["accion"]=='ELIMINAR')?"readonly":"";
	$intentando="<h3>Intentando $_GET[accion] el registro:</h3>";
	$exitosamente="<h3 style='color: red;'>$_GET[accion] exitosamente</h3>";
	$error="<h3 style='color: red;'>ERROR: No se pudo $_GET[accion]</h3>";
	if(isset($_POST['CONFIRMAR']) AND $_POST['CONFIRMAR']=="ACEPTAR")
		{
			$incompleto=FALSE;
			foreach ($_POST as  $campo => $valor){
					if ($campo!="CONFIRMAR"){
						if($valor==="0"||$valor)//LA UNICA FORMA QUE AGARRA LOS CEROS
							$parejas[$campo]=$valor;
						else
							$incompleto=TRUE;
					}
			}
			echo	$intentando;

			Print_r($parejas);echo "<br>";

			if($incompleto){
				echo	"<h3 style='color: red;'>Error Registros incompletos!</h3>";
				}
			else{
					switch ($_GET["accion"]) {
						case 'EDITAR':
							echo "REEMPLAZA: ";Print_r($anteriores);echo "<br>";
							$result=$wpdb->update($tabla,$parejas,$anteriores);
							break;

						case 'ELIMINAR':
							$result=$wpdb->delete($tabla,$parejas);
							break;

						case 'AGREGAR':
							$result=$wpdb->insert($tabla,$parejas);
							break;
					}

					if($result > 0){
						echo $exitosamente;
						$registros[0]=$parejas;
						$esteregistro=(array) $registros[0];
						$_GET["sel"]=base64_encode(json_encode(compact("tabla","esteregistro")));
					}
					else{echo $error;}
					echo "</br>";
				}
		}

		//FORMULARIO
		echo	"<h3 style='color: red;'>SEGURO DESEA $_GET[accion] EL SIGUIENTE REGISTRO:</h3>";
			if (isset($registros[0])){ ?>
				<form action='?<?php echo http_build_query($_GET)?>' id='usrform' method='post'>
						<?php
						foreach($registros[0] as $campo => $valor){
										echo '<p><label class="formulario" for="'.$campo.'">'.$campo.'</label>';
										echo "<input class='formulario' type='text' id='$campo' name='$campo' value='$valor'/ ".$deshabilitar."></p>";
									}
						?>
						<a class='botonpeque' href='<?php echo "?page=$_GET[page]" ?>'>< VOLVER</a>
						<input name="CONFIRMAR" class="boton" type="submit" value="ACEPTAR"/>
				</form>
					 <?php
				}
			else
				echo	"<h3 style='color: red;'>NO EXISTE EL REGISTRO</h3>";

	return ob_get_clean();
}




function agregar_registro ($tabla){
	if(isset($_POST['opcion'])AND$_POST['opcion']=="Enviar")
	{
		insertar_plano($tabla);
	}
	if(isset($_POST['CONFIRMAR'])AND$_POST['CONFIRMAR']=="ACEPTAR")
		{
		guardar_registro($tabla);
		}
	global $wpdb;$wpdb->show_errors();	$consulta="DESCRIBE $tabla";	$registros=$wpdb->get_results($consulta);
	$primercampo = $registros[0] -> Field;
	$ultimo=$wpdb->get_var("SELECT MAX($primercampo) FROM $tabla");
	$ultimo++;
	$botonVolver = <<<HTML
<a class='botonpeque' href='?page=$_GET[page]'>< VOLVER</a>
HTML;


	echo "<h2>Puedes agregar registros de 2 maneras:</h2>";

	echo "<h3>1-LLenando el siguiente formulario(tener en cuenta el tipo de dato)</h3>";
		echo '<form action=?'.http_build_query($_GET).' id="usrform" method="post">';
				$contador=1;
				foreach($registros as $registro){
					echo '<p><label class="formulario" for="'.$registro -> Field.'">'.$registro -> Field.' ('.$registro -> Type.')</label>';
					echo '<input class="formulario" type="text" id="'.$registro -> Field.'" name="'.$registro -> Field.'" value="'.(($contador=="1")?$ultimo:'').'"/></p>';
					$contador++;
					}
	echo '<input name="CONFIRMAR" class="boton" type="submit" value="ACEPTAR"/></form>';


	echo "<h3>2-Copiando y pegando un archivo plano separado por comas (Cada l&iacute;nea cuenta como un registro, FUNCION SOLO PARA USO TECNICO)</h3>";
	echo '<form action='.http_build_query($_GET).'id="usrform" method="post">
		<textarea id="plano" name="plano" rows="4" cols="50"></textarea></br>
		Separador (Predeterminado "coma"): <input type="text" id="separador" name="separador" value=","></br>
		<input name="opcion" class="boton" type="submit" value="Enviar"/>'.$botonVolver.'
		</form>';
		return ob_get_clean();
}

function guardar_registro($tabla){
	global $wpdb;$wpdb->show_errors();
	echo "GUARDAR";
			$incompleto=FALSE;
	   foreach ($_POST as  $campo => $valor){
	    if ($campo!="CONFIRMAR"){
			if($valor==="0"||$valor)
			$parejas[$campo]=$valor;
			else
			$incompleto=TRUE;
			}
		}
		echo	"<h3>Intentando inserci&oacute;n del registro:</h3>";
		Print_r($parejas);
		if($incompleto){
			echo	"<h3>Error Registros incompletos!</h3>";
			}
		else{
				$result=$wpdb->insert($tabla,$parejas);
				if($result > 0){echo "<h3 style='color: red;'>Insertada exitosamente</h3>";}
				echo "</br>";
			}

	}

function insertar_plano($tabla){
	global $wpdb;$wpdb->show_errors();	$consulta="DESCRIBE $tabla";	$registros=$wpdb->get_results($consulta);

	echo "ENVIAR";
	$incompleto=FALSE;

 		foreach($registros as $registro){

				$campos[]= $registro -> Field;

		}

		$lineas=explode("\r\n", $_POST['plano']);

			foreach ($lineas as $linea){
				if ($linea)
					$valores[]=explode($_POST['separador'], $linea);

			}

			if (isset($valores)AND$valores)
			foreach ($valores as $valor) {
				echo	"<h3>Intentando inserci&oacute;n del registro:</h3>";
						for($x=0; $x < count($campos); $x++){
							echo "<br>$campos[$x] > ";
							if(isset($valor[$x])AND$valor[$x]){
								$parejas[$campos[$x]]= $valor[$x];echo " $valor[$x]";
							}
							else
								$incompleto=TRUE;
						}

						if($incompleto){
							echo	"<h3>Error de Registro: Archivo plano erroneo, revise separador y que el numero de columnas coincida con la tabla!</h3>";
							$incompleto=FALSE;
						}
						else{
							$wpdb->insert($tabla,$parejas);
							if($wpdb->insert_id){echo "<h3>Insertada exitosamente</h3>";}
							echo "</br>";
						}
	  		}
	}

?>
