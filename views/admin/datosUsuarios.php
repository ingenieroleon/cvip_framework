<h3><?php _e('Info empresa', 'textdomain'); ?></h3>

<table class="form-table">
    <tr>
      <th>
          <label for="nombreempresa"><?php _e('Nombre empresa', 'textdomain'); ?></label>
      </th>
      <td>
          <input type="text" name="nombreempresa" id="nombreempresa" value="<?php echo esc_attr( get_the_author_meta( 'nombreempresa', @$user->ID ) ); ?>" class="regular-text" />
          <p class="description"><?php _e('Ingresa el nombre de empresa que puede ver.', 'textdomain'); ?></p>
      </td>
    </tr>
    <tr>
      <th>
        <label for="idempresa"><?php _e('ID Empresa', 'textdomain'); ?></label>
      </th>
      <td>
        <input type="text" name="idempresa" id="idempresa" value="<?php echo esc_attr( get_the_author_meta( 'idempresa', @$user->ID ) ); ?>" class="regular-text" />
        <p class="description"><?php _e('Ingresa el ID de empresa que puede ver.', 'textdomain'); ?></p>
      </td>
    </tr>
    <tr>
      <th>
        <label for="permisos"><?php _e('Permisos de usuario', 'textdomain'); ?></label>
      </th>
      <td>
        <input type="text" name="permisos" id="permisos" value="<?php echo esc_attr( get_the_author_meta( 'permisos', @$user->ID ) ); ?>" class="regular-text" />
        <p class="description"><?php _e('Ingrese las letras de los permisos en mayúsculas separados por comas EJ: H,I,P,R,X,Z', 'textdomain'); ?></p>
      </td>
    </tr>
    <tr>
      <th>
        <label for="prueba2"><?php _e('Prueba 2', 'textdomain'); ?></label>
      </th>
      <td>
        <input type="text" name="prueba2" id="prueba2" value="<?php echo esc_attr( get_the_author_meta( 'prueba2', @$user->ID ) ); ?>" class="regular-text" />
        <p class="description"><?php _e('Ingrese las letras de los prueba2 en mayúsculas separados por comas EJ: H,I,P,R,X,Z', 'textdomain'); ?></p>
      </td>
    </tr>

</table>
