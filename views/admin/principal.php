<div class="titulo-plugin">
  <h3><?=ucfirst(get_admin_page_title()); ?></h3>
</div>

<div class="fila items">
    <?php foreach ($datos as $sub_page): ?>
      <div class="celda <?=str_replace('cvip_', '', $sub_page); ?>">
        <a href="<?="?page=$sub_page"; ?>">
          <div class="icono"></div>
          <div class="botonTitulo">
            <?=ucfirst($sub_page); ?>
          </div>
         </a>
      </div>
    <?php endforeach; ?>

    <div class="celda <?='salida'; ?>">
      <a href="<?php echo wp_logout_url( home_url() ); ?>">
        <div class="icono"></div>
        <div class="botonTitulo">
          <?='Salir'; ?>
        </div>
       </a>
    </div>

</div>
